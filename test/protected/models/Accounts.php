<?php

/**
 * This is the model class for table "accounts".
 *
 * The followings are the available columns in table 'accounts':
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $company
 */
class Accounts extends CActiveRecord{

	public function tableName(){
		return 'accounts';
	}

	public function rules(){
		return array(
			array('email, password, company', 'required'),
			array('email', 'length', 'max'=>100),
			array('password', 'length', 'min'=>32, 'max'=>32),
			array('company', 'length', 'max'=>120),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, company', 'safe', 'on'=>'search'),
		);
	}

	public function relations(){
		return [];
	}

	public function attributeLabels(){
		return array(
			'id' => 'ID',
			'email' => 'Логин (адрес email)',
			'password' => 'Пароль',
			'company' => 'Название компании',
		);
	}

	public function search(){
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id,true);
		$criteria->compare('email',$this->email,true);
		# $criteria->compare('password',$this->password,true);
		$criteria->compare('company',$this->company,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__){
		return parent::model($className);
	}
}
