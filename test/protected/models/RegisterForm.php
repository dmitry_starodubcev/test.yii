<?php

class RegisterForm extends CFormModel{
	public $email;
	public $password;
	public $password2;
	public $company;

	public function rules(){
		return array(
			// username and password are required
			['email, password,password2, company', 'required'],
			['email','email'],
			['password', 'compare', 'compareAttribute'=>'password2'],
			['email', 'unique', 'className'=>'Accounts'],
		);
	}

	public function attributeLabels(){
		return array(
			'email'=>'Адрес электронной почты',
			'password'=>'Пароль',
			'password2'=>'Повторите пароль',
			'company'=>'Название организации'
		);
	}
}

?>