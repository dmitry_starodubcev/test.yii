<?php /* @var $this Controller */ ?>
<!doctype html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/css/bootstrap.min.css" media="screen">

	<style type="text/css">
		span.required, .errorMessage{
			color: red;
		}
		.rememberMe input{
			float: left;
			margin-right: 10px;
		}
	</style>
</head>
<body>
	<div class="container">

		<header>
			<div class="navbar">
				<nav class="navbar-inner">
					<a class="brand" href="#"><?php echo CHtml::encode(Yii::app()->name); ?></a>
					<?php $this->widget('zii.widgets.CMenu', array(
						'items' => array(
							array('label' => 'Главная', 'url' => array('/site/index')),
							array('label' => 'Вход', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
							array('label' => 'Выход (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
						),
						'htmlOptions' => array( 'class' => 'nav'),
					)); ?>
				</nav>
			</div>
		</header>

		<nav>
			<?php if (isset($this->breadcrumbs)): ?>
				<?php $this->widget('zii.widgets.CBreadcrumbs', array(
						'links'=>$this->breadcrumbs,
						'htmlOptions' => array('class' => 'breadcrumb'),
				)); ?>
			<?php endif ?>
		</nav>

		<section class="row">
			<?php echo $content; ?>
		</section>

		<footer></footer>

	</div>
</body>
</html>
