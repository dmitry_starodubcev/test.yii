<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Вход';
$this->breadcrumbs=[
	'Вход',
];
?>


<h2>Вход</h2>

<p>Заполните форму данными, используемыми при регистрации</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="alert">
		Поля со звёздочкой обязательны.
		<span class="required">*</span>
	</p>

	<div class="row">
		<div class="span12">
			<?php echo $form->labelEx($model,'username'); ?>
			<?php echo $form->textField($model,'username'); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>

		<div class="span12">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password'); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>

		<div class="span12 rememberMe">
			<?php echo $form->checkBox($model,'rememberMe'); ?>
			<?php echo $form->label($model,'rememberMe'); ?>
			<?php echo $form->error($model,'rememberMe'); ?>
		</div>
	</div>

	<div class="row">
		<div class="span3">
			<?php echo CHtml::submitButton('Вход',['class'=>'btn btn-success btn-block']); ?>
		</div>
		<div class="span3">
			<a class='btn btn-info btn-block' href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/register">Регистрация</a>
		</div>
	</div>


<?php $this->endWidget(); ?>
</div><!-- form -->
