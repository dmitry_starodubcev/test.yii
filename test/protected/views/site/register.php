<?php
/* @var $this AccountsController */
/* @var $model Accounts */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Регистрация';
$this->breadcrumbs=[
'Регистрация',
];

?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/js/bootstrap.min.js"></script>


<script type="text/javascript">
	var companies = [];
	$(function(){
		$('#RegisterForm_company').keyup(function(){
			var usertext = $(this).val();
			$.get('<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/getCompanies',{company:usertext},function(data){
				companies = data;
			},'json');
		});
		$('.typeahead').typeahead({
			source:function(){ return companies; }
		});
	});
</script>

<h2>Регистрация</h2>

<p>Регистрация на сайте</p>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'accounts-register-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="alert">
		Поля со звёздочкой обязательны.
		<span class="required">*</span>
	</p>

	<div class="row">
		<?php echo $form->errorSummary($model); ?>

		<div class="span12">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email'); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>

		<div class="span12">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password'); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>

		<div class="span12">
			<?php echo $form->labelEx($model,'password2'); ?>
			<?php echo $form->passwordField($model,'password2'); ?>
			<?php echo $form->error($model,'password2'); ?>
		</div>

		<div class="span12">
			<?php echo $form->labelEx($model,'company'); ?>
			<?php echo $form->textField($model,'company',['class'=>"typeahead"]); ?>
			<?php echo $form->error($model,'company'); ?>
		</div>
	</div>
	<div class="row">
		<div class="span3">
			<?php echo CHtml::submitButton('Зарегистрировать',['class'=>'btn btn-success btn-block']); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->